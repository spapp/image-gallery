/**
 * image gallery jQuery plugin.
 *
 * @category example
 * @author Sandor Papp <spapp@spappsite.hu>
 * @copyright Copyright (c) 2013, Sandor Papp
 * @license http://creativecommons.org/licenses/by-nc-nd/3.0/deed.hu Creative Commons
 */
(function ($) {
    /**
     * Languages links handling.
     *
     * @param {Object} aOptions
     *                  - {String} links A string containing a selector expression to match elements against.
     *                  - {String} language current language
     *                  - {String} activeClass css class
     *                  - {Function} onChange A callback function. It will call if the language changed.
     * @constructor
     */
    var Langs = function Langs(aOptions) {
        var _this = this;
        Object.defineProperty(this, 'links', {
            enumerable: true,
            configurable: false,
            writable: false,
            /**
             * Languages links.
             *
             * @type {jQuery}
             */
            value: $(aOptions.links)
        });

        Object.defineProperty(this, 'language', {
            enumerable: false,
            configurable: true,
            writable: true,
            /**
             * Current language.
             *
             * @type {String}
             */
            value: aOptions.language
        });
        Object.defineProperty(this, 'activeClass', {
            enumerable: false,
            configurable: true,
            writable: true,
            /**
             * Current language link css alass.
             *
             * @type {String}
             */
            value: aOptions.activeClass
        });

        this.getLinks().click(function () {
            _this.language = $(this).attr('lang');
            _this.refreshLinks();
            if (typeof aOptions.onChange === 'function') {
                aOptions.onChange();
            }
        });

        this.refreshLinks();
    };

    Object.defineProperty(Langs.prototype, 'getLanguage', {
        enumerable: false,
        configurable: false,
        writable: false,
        /**
         * Returns current language.
         *
         * @return {jQuery}
         */
        value: function Langs_getLanguage() {
            return this.language;
        }
    });

    Object.defineProperty(Langs.prototype, 'getLinks', {
        enumerable: false,
        configurable: false,
        writable: false,
        /**
         * Returns languages links.
         *
         * @return {jQuery}
         */
        value: function Langs_getLinks() {
            return this.links;
        }
    });

    Object.defineProperty(Langs.prototype, 'refreshLinks', {
        enumerable: false,
        configurable: false,
        writable: false,
        /**
         * Update the language links in the DOM.
         * Add or Remove active css class.
         *
         * @return {Langs}
         */
        value: function Langs_refreshLinks() {
            var _this = this;
            this.getLinks().each(function (index, link) {
                if ($(link).attr('lang') === _this.getLanguage()) {
                    $(link).addClass(_this.activeClass);
                } else {
                    $(link).removeClass(_this.activeClass);
                }
            });
            return this;
        }
    });

    /**
     * The caption of the image and its management.
     *
     * @param {Object} aOptions
     *                      - {String} img A string containing a selector expression to match an image element.
     *                      - {String} label A string containing a selector expression to match an image element against.
     * @constructor
     */
    var Figure = function Figure(aOptions) {
        Object.defineProperty(this, 'img', {
            enumerable: true,
            configurable: false,
            writable: false,
            /**
             * Image object(s). That displays the image.
             *
             * @type {jQuery}
             */
            value: $(aOptions.img)
        });
        Object.defineProperty(this, 'label', {
            enumerable: true,
            configurable: false,
            writable: false,
            /**
             * Image label object(s). That displays the image label if it is exists.
             *
             * @type {jQuery}
             */
            value: $(aOptions.label)
        });
    };

    Object.defineProperty(Figure.prototype, 'setImage', {
        enumerable: true,
        configurable: false,
        writable: false,
        /**
         * The image src attribute is set to.
         *
         * @param {String} aSrc the iamge url
         * @return {Figure}
         */
        value: function Figure_setImage(aSrc) {
            var img = this.img;
            img.fadeTo('slow', 0, function () {
                img.attr('src', aSrc);
                img.fadeTo('slow', 1);
            });
            return this;
        }
    });
    Object.defineProperty(Figure.prototype, 'setLabel', {
        enumerable: false,
        configurable: false,
        writable: false,
        /**
         * The image caption is set to.
         *
         * @param {String} aLabel the iamge caption
         * @return {Figure}
         */
        value: function Figure_setLabel(aLabel) {
            this.label.html(aLabel);
            return this;
        }
    });

    /**
     * The image gallery object.
     *
     * @param {String|DOMElement|jQuery} aParent the gallery root element
     * @param {Object} aOptions the gallery options
     * @see Gallery.defaultOptions
     * @constructor
     */
    var Gallery = function Gallery(aParent, aOptions) {
        var _this = this;
        Object.defineProperty(this, 'options', {
            enumerable: true,
            configurable: false,
            writable: false,
            /**
             * The gallery options.
             *
             * @type {Object}
             */
            value: aOptions
        });

        Object.defineProperty(this, 'lang', {
            enumerable: true,
            configurable: false,
            writable: false,
            /**
             * Languages ​​management.
             *
             * @type {Langs}
             * @see Langs
             */
            value: new Langs({
                                 language: aOptions.language,
                                 links: aOptions.langLinks,
                                 activeClass: aOptions.activeClass,
                                 onChange: this.languageOnChange.bind(this)
                             })
        });

        Object.defineProperty(this, 'figure', {
            enumerable: true,
            configurable: false,
            writable: false,
            /**
             * The caption of the image and its management.
             *
             * @type {Figure}
             * @see Figure
             */
            value: new Figure(aOptions)
        });


        if (typeof aOptions.getMaxIndex === 'function') {
            aOptions.getMaxIndex(this.setMaxIndex.bind(this));
        }

        $(aParent).find(aOptions.back).click(function(){
            _this.move.call(_this, Gallery.DIRECTION_BACK);
            return false;
        });
        $(aParent).find(aOptions.next).click(function(){
            _this.move.call(_this, Gallery.DIRECTION_NEXT);
            return false;
        });
    };

    Object.defineProperty(Gallery, 'DIRECTION_BACK', {
        enumerable: true,
        configurable: false,
        writable: false,
        /**
         * @type {Number}
         * @constant
         */
        value: -1
    });

    Object.defineProperty(Gallery, 'DIRECTION_NEXT', {
        enumerable: true,
        configurable: false,
        writable: false,
        /**
         * @type {Number}
         * @constant
         */
        value: 1
    });

    Object.defineProperty(Gallery, 'DIRECTION_STAY', {
        enumerable: true,
        configurable: false,
        writable: false,
        /**
         * @type {Number}
         * @constant
         */
        value: 0
    });

    Object.defineProperty(Gallery, 'defaultOptions', {
        enumerable: false,
        configurable: false,
        writable: false,
        /**
         * The gallery default options.
         *
         * @type {Object}
         */
        value: {
            disabledClass: 'disabled',
            activeClass: 'active',
            language: 'hu',
            back: 'a.back',
            next: 'a.next',
            img: 'figure img',
            label: 'figure figcaption',
            langLinks: 'a[lang]'
        }
    });

    Object.defineProperty(Gallery.prototype, 'index', {
        enumerable: false,
        configurable: false,
        writable: true,
        /**
         * The gallery current image index.
         *
         * @type {Number}
         */
        value: 0
    });

    Object.defineProperty(Gallery.prototype, 'setIndex', {
        enumerable: true,
        configurable: false,
        writable: false,
        /**
         * Set the gallery current image index.
         *
         * @param {Number} aIndex image index
         * @return {Gallery}
         */
        value: function Gallery_setIndex(aIndex) {
            this.index = aIndex;
            return this;
        }
    });

    Object.defineProperty(Gallery.prototype, 'getIndex', {
        enumerable: true,
        configurable: false,
        writable: false,
        /**
         * Return the gallery current image index.
         *
         * @return {Number}
         */
        value: function Gallery_getIndex() {
            return this.index;
        }
    });

    Object.defineProperty(this, 'maxIndex', {
        enumerable: false,
        configurable: false,
        writable: true,
        /**
         * The gallery maximum image index.
         *
         * @type {Number}
         */
        value: 0
    });

    Object.defineProperty(Gallery.prototype, 'setMaxIndex', {
        enumerable: true,
        configurable: false,
        writable: false,
        /**
         * Set the gallery maximum image index.
         *
         * @param {Number} aMaxIndex maximum image index
         * @return {Gallery}
         */
        value: function Gallery_setMaxIndex(aMaxIndex) {
            this.maxIndex = aMaxIndex;
            return this;
        }
    });

    Object.defineProperty(Gallery.prototype, 'getMaxIndex', {
        enumerable: true,
        configurable: false,
        writable: false,
        /**
         * Return the gallery maximum image index.
         *
         * @return {Number}
         */
        value: function Gallery_getMaxIndex() {
            return this.maxIndex;
        }
    });

    Object.defineProperty(Gallery.prototype, 'moveIndex', {
        enumerable: true,
        configurable: false,
        writable: false,
        /**
         * The current image index is moving in the specified direction.
         *
         * @param {Number} aDirection
         * @see Gallery.DIRECTION_BACK
         * @see Gallery.DIRECTION_NEXT
         * @see Gallery.DIRECTION_STAY
         * @return {Gallery}
         */
        value: function Gallery_moveIndex(aDirection) {
            aDirection = parseInt(aDirection, 10);

            switch (aDirection) {
                case Gallery.DIRECTION_BACK:
                case Gallery.DIRECTION_NEXT:
                    this.index += aDirection;
                    break;
            }

            if (this.index < 0) {
                this.index = this.getMaxIndex();
            } else if (this.index > this.getMaxIndex()) {
                this.index = 0;
            }

            return this;
        }
    });

    Object.defineProperty(Gallery.prototype, 'languageOnChange', {
        enumerable: true,
        configurable: false,
        writable: false,
        /**
         * Language on change event.
         *
         * @return {Gallery}
         */
        value: function Gallery_languageOnChange() {
            if (typeof this.options.getLabel === 'function') {
                this.options.getLabel(this.getIndex(), this.lang.getLanguage(), this.setImage.bind(this));
            }
            return this;
        }
    });

    Object.defineProperty(Gallery.prototype, 'setImage', {
        enumerable: true,
        configurable: false,
        writable: false,
        /**
         * Set and display current image and its label.
         *
         * @param {Object} aData image data
         *                  - {String} src   image url
         *                  - {String} label image label
         * @return {Gallery}
         */
        value: function Gallery_setImage(aData) {
            if (aData) {
                if (typeof aData.src === 'string') {
                    this.figure.setImage(aData.src);
                }
                if (typeof aData.label === 'string') {
                    this.figure.setLabel(aData.label);
                }
            }
            return this;
        }
    });

    Object.defineProperty(Gallery.prototype, 'move', {
        enumerable: false,
        configurable: false,
        writable: false,
        /**
         * The next or previous image acquisition.
         *
         * @param {Number} aDirection
         * @see Gallery.DIRECTION_BACK
         * @see Gallery.DIRECTION_NEXT
         * @see Gallery.DIRECTION_STAY
         * @return {Gallery}
         */
        value: function Gallery_move(aDirection) {
            this.moveIndex(aDirection);
            if (typeof this.options.getImage === 'function') {
                this.options.getImage(this.getIndex(), this.lang.getLanguage(), this.setImage.bind(this));
            }
            return this;
        }
    });
    /**
     * Defined imgGallery jQuery plugin.
     *
     * @param {object} aOptions
     * @see Gallery.defaultOptions
     */
    $.fn.imgGallery = function (aOptions) {
        new Gallery(this, $.extend({}, Gallery.defaultOptions, (aOptions || {}))).move();
    }
})(jQuery);