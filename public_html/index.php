<?php
//date_default_timezone_set('Europe/Budapest');

/**
 * Define path to application directory.
 *
 * @global string
 */
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'application'));

/**
 * Define application environment.
 * So you can use the application with different settings (other database, highest log level etc...) .
 * The URL is a special prefix to be used. It is 'dev-'.
 *
 * @global string
 */
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

if ('production' !== APPLICATION_ENV) {
    error_reporting(E_ALL ^ E_DEPRECATED);
    ini_set('display_errors', 'On');
}

/**
 * Typically, you will also want to add your library/ directory
 * to the include_path, particularly if it contains your ZF installed
 */
set_include_path(implode(PATH_SEPARATOR,
                         array(
                             realpath(dirname(__DIR__) . '/library'),
                             APPLICATION_PATH,
                             get_include_path(),
                         )));
/**
 * register Zend autoloader
 */
require_once 'Zend/Loader/AutoloaderFactory.php';
Zend_Loader_AutoloaderFactory::factory(
    array(
        'Zend_Loader_StandardAutoloader' => array(
            'prefixes' => array(
                'Zend' => realpath(__DIR__ . '/../library/Zend'),
                'Model' => APPLICATION_PATH . '/models'
            ),
            'fallback_autoloader' => true
        )
    )
);
/**
 * Create application, bootstrap, and run
 */
$application = new Zend_Application(APPLICATION_ENV, APPLICATION_PATH . '/configs/application.ini');
$application->bootstrap()->run();
