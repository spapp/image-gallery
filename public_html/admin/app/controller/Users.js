Ext.define('IG.controller.Users', {
    extend: 'Ext.app.Controller',

    models: ['User'],

    views: [
        'user.Auth'
    ],

    init: function () {
        this.control({
            'userauth button[action=auth]': {
                click: this.auth
            }
        });
    },

    auth: function (button) {
        var values = button.up('form').getValues();
        IG.authUser = Ext.create('IG.model.User', values);

        button.up('window').close();

        Ext.create('Ext.container.Viewport', {
            layout: 'fit',
            items: [
                {
                    xtype: 'imagelist'
                }
            ]
        });
    }
});