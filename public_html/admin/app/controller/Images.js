Ext.define('IG.controller.Images', {
    extend: 'Ext.app.Controller',

    stores: [
        'Images'
    ],

    models: ['Image'],

    views: [
        'image.List',
        'image.Edit',
        'image.Upload'
    ],

    init: function () {
        this.control({
            'viewport > panel': {
                render: this.onPanelRendered
            },
            'imagelist': {
                itemdblclick: this.editImage
            },
            'imagelist dataview': {
                drop: this.dropImage
            },
            'imageedit button[action=save]': {
                click: this.updateImage
            },
            'imageedit button[action=delete]': {
                click: this.deleteImage
            },
            'imagelist button[action=upload]': {
                click: this.uploadImage
            },
            'imageupload button[action=uploadimage]': {
                click: this.upload
            },
            'imageupload button[action=resetupload]': {
                click: this.resetUpload
            }
        });
    },

    onPanelRendered: function () {
        var _this = this;
        this.getImagesStore().on('beforeload', function (s) {
            s.proxy.headers = _this.getAuthHeaders();
        });
        this.getImagesStore().load();
    },

    getAuthHeaders: function () {
        return {
            'X-auth-username':IG.authUser.get('name'),
            'X-auth-password':IG.authUser.get('password')
        };
    },

    dropImage: function () {
        var store = this.getImagesStore(), order = [], _this = this;
        store.each(function (rec) {
            order.push(rec.getId());
        });
        Ext.Ajax.request({
            url: '/images/move',
            params: {
                order: Ext.encode(order)
            },
            headers: this.getAuthHeaders(),
            failure: function () {
                _this.getImagesStore().reload();
            }
        });
    },

    upload: function (button) {
        var form = button.up('form').getForm(),
            win = button.up('window'),
            _this = this;
        if (form.isValid()) {
            form.submit({
                url: '/upload',
                waitMsg: 'Uploading a image...',
                headers: this.getAuthHeaders(),
                success: function (fp, o) {
                    Ext.Msg.show({
                        title: 'Success',
                        msg: 'Processed file on the server.',
                        minWidth: 200,
                        modal: true,
                        icon: Ext.Msg.INFO,
                        buttons: Ext.Msg.OK
                    });
                    win.close();
                    _this.getImagesStore().reload();
                }
            });
        }
    },

    resetUpload: function (button) {
        button.up('form').getForm().reset();
    },

    editImage: function (grid, record) {
        var view = Ext.widget('imageedit');
        view.down('form').loadRecord(record);
    },

    updateImage: function (button) {
        var win = button.up('window'),
            form = win.down('form'),
            record = form.getRecord(),
            values = form.getValues(),
            _this = this;

        record.set(values);
        win.close();

        Ext.Ajax.request({
            url: '/image/update',
            headers: this.getAuthHeaders(),
            params: {
                image: Ext.encode(values)
            },
            success: function () {
                _this.getImagesStore().sync();
            },
            failure: function () {
                _this.getImagesStore().reload();
            }
        });
    },
    deleteImage: function (button) {
        var win = button.up('window'),
            form = win.down('form'),
            values = form.getValues(),
            _this = this;


        Ext.Ajax.request({
            url: '/image/delete/id/' + values.id,
            headers: this.getAuthHeaders(),
            success: function () {
                _this.getImagesStore().reload();
                win.close();
                _this.dropImage();
            },
            failure: function () {
                _this.getImagesStore().reload();
                win.close();
                _this.dropImage();
            }
        });
    },
    uploadImage: function (button) {
        var view = Ext.widget('imageupload');
        view.show();
    }
});