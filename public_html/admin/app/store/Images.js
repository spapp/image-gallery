Ext.define('IG.store.Images', {
    extend: 'Ext.data.Store',
    model: 'IG.model.Image',
    autoLoad: false,

    proxy: {
        type: 'ajax',
        url: '/images/get',
        reader: {
            type: 'json',
            root: 'images',
            successProperty: 'success'
        }
    }
});