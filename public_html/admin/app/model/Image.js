Ext.define('IG.model.Image', {
    extend: 'Ext.data.Model',
    fields: ['name', 'label_en', 'label_hu']
});