Ext.define('IG.view.user.Auth', {
    extend: 'Ext.window.Window',
    alias: 'widget.userauth',

    title: '',
    width: 300,
    closable: false,
    modal: true,

    initComponent: function () {
        this.items = {
            xtype: 'form',
            padding: '5',
            border: false,
            bodyPadding: '10 10 0',
            defaults: {
                anchor: '100%',
                allowBlank: false,
                msgTarget: 'side',
                labelWidth: 100
            },
            items: [
                {
                    xtype: 'textfield',
                    name : 'name',
                    fieldLabel: 'Username'
                },
                {
                    xtype: 'textfield',
                    name : 'password',
                    inputType: 'password',
                    fieldLabel: 'Password'
                }
            ]
        };

        this.items.buttons = [
            {
                text: 'Ok',
                action: 'auth'
            }
        ];

        this.callParent(arguments);
    }
})
;