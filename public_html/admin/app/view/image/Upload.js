Ext.define('IG.view.image.Upload', {
    extend: 'Ext.window.Window',
    alias: 'widget.imageupload',

    title: 'Upload Image',
    width: 500,
    modal: true,

    initComponent: function () {
        this.items = {
            xtype: 'form',
            padding: '5',
            border: false,
            bodyPadding: '10 10 0',
            defaults: {
                anchor: '100%',
                allowBlank: false,
                msgTarget: 'side',
                labelWidth: 50
            },
            items: [
                {
                    xtype: 'textfield',
                    name : 'label_en',
                    required: false,
                    fieldLabel: 'Label (en)'
                },
                {
                    xtype: 'textfield',
                    name : 'label_hu',
                    required: false,
                    fieldLabel: 'Label (hu)'
                },{
                    xtype: 'filefield',
                    id: 'form-file',
                    emptyText: 'Select an image',
                    fieldLabel: 'Image',
                    name: 'image-path',
                    buttonText: '',
                    buttonConfig: {
                        iconCls: 'upload-icon'
                    }
                }
            ]
        };

        this.items.buttons = [
            {
                text: 'Upload',
                action: 'uploadimage'
            },
            {
                text: 'Reset',
                action: 'resetupload'
            }
        ];

        this.callParent(arguments);
    }
})
;