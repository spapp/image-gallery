Ext.define('IG.view.image.Edit', {
    extend: 'Ext.window.Window',
    alias: 'widget.imageedit',

    title: 'Edit Image',
    layout: 'fit',
    modal: true,
    autoShow: true,

    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                padding: '5',
                border: false,
                items: [
                    {
                        xtype: 'hidden',
                        name : 'id'
                    },
                    {
                        xtype: 'textfield',
                        name : 'name',
                        readOnly: true,
                        disabled: true,
                        fieldLabel: 'Name'
                    },{
                        xtype: 'textfield',
                        name : 'label_en',
                        fieldLabel: 'Label (en)'
                    },{
                        xtype: 'textfield',
                        name : 'label_hu',
                        fieldLabel: 'Label (hu)'
                    }
                ]
            }
        ];

        this.buttons = [
            {
                text: 'Save',
                action: 'save'
            },{
                text: 'Delete',
                action: 'delete'
            },
            {
                text: 'Cancel',
                scope: this,
                handler: this.close
            }
        ];

        this.callParent(arguments);
    }
});