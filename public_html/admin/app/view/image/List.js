Ext.define('IG.view.image.List', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.imagelist',

    title: 'All Images',
    store: 'Images',
    viewConfig: {
        plugins: {
            ptype: 'gridviewdragdrop',
            dragText: 'Reorder rows.'
        }
    },

    tbar: [
        {text: 'Upload', action: 'upload'},
        '-',
        {text: 'Export (csv)',  href:'/images/export/type/csv'},
        '-',
        {text: 'Export (xml)', href:'/images/export/type/xml'}
    ],

    initComponent: function () {

        this.columns = [
            {header: 'Name', dataIndex: 'name', flex: 1},
            {header: 'Label (en)', dataIndex: 'label_en', flex: 1},
            {header: 'Label (hu)', dataIndex: 'label_hu', flex: 1}
        ];

        this.callParent(arguments);
    }
});