Ext.application({
    requires: ['Ext.container.Viewport'],
    name: 'IG',

    appFolder: 'app',

    controllers: [
        'Images',
        'Users'
    ],

    launch: function () {
        var view = Ext.widget('userauth');
        view.show();
    }
});