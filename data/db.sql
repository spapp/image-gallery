--
-- Adatbázis: `image_gallery`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet: `image`
--

CREATE TABLE IF NOT EXISTS `image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `label_en` varchar(255) NOT NULL,
  `label_hu` varchar(255) NOT NULL,
  `orderby` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orderby_index` (`orderby`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;