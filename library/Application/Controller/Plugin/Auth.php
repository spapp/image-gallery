<?php
/**
 * Controller auth plugin
 */
class Application_Controller_Plugin_Auth extends Zend_Controller_Plugin_Abstract {
    /**
     * auth config
     *
     * @var Zend_Config
     */
    protected $config = null;

    /**
     * The application auth config.
     * You can find the application config file.
     * For example (application.ini):
     * <code>
     * ; auth public actions
     * auth.rights.public.<controller name>.<action name> = true
     * ....
     * ; auth user data
     * auth.username = "username"
     * auth.password = "password"
     * </code>
     *
     * @return Zend_Config the auth config
     */
    protected function getAuthConfig() {
        if (null === $this->config) {
            $options = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
            if (isset($options['auth'])) {
                $options = $options['auth'];
            }
            $this->config = new Zend_Config($options);
        }
        return $this->config;
    }

    /**
     * The controller action is a public method.
     *
     * @param string $aControllerName
     * @param string $aActionName
     *
     * @return bool
     */
    protected function isPublic($aControllerName, $aActionName) {
        $config = $this->getAuthConfig();
        if (isset($config->{$aControllerName}) and
            isset($config->{$aControllerName}->{$aActionName}) and
                $config->{$aControllerName}->{$aActionName} === true
        ) {
            return true;
        }
        return false;
    }

    /**
     * The two values is equal.
     *
     * @param string $aValueA
     * @param string $aValueB
     *
     * @return bool
     */
    protected function isEqual($aValueA, $aValueB) {
        return (trim((string)$aValueA) === trim((string)$aValueB));
    }

    /**
     * Username and password pair is valid.
     *
     * @return bool
     */
    protected function authorize() {
        $username = $this->getRequest()->getHeader($this->getAuthConfig()->username->header);
        $password = $this->getRequest()->getHeader($this->getAuthConfig()->password->header);

        return ($this->isEqual($username, $this->getAuthConfig()->username->value) and
            $this->isEqual($password, $this->getAuthConfig()->password->value));
    }
    /**
     * Called after Zend_Controller_Router exits.
     *
     * Called after Zend_Controller_Front exits from the router.
     *
     * @param  Zend_Controller_Request_Abstract $aRequest
     * @see Zend_Controller_Plugin_Abstract
     * @return void
     */
    public function routeShutdown(Zend_Controller_Request_Abstract $aRequest) {
        if (false === $this->isPublic($aRequest->getControllerName(),
                                      $aRequest->getActionName()) and false === $this->authorize()
        ) {
            $this->getResponse()->setHttpResponseCode(401);
            $this->getResponse()->sendHeaders();
            exit;
        }
    }
}