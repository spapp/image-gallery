<?php

class ImageController extends Zend_Controller_Action {
    public function init() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
    }

    public function getAction() {
        $id = $this->getParam('id', 0);
        $lang = $this->getParam('lang', 'en');
        $imageMapper = Model_Mapper_Image::getInstance();
        $image = $imageMapper->fetchRow(array(
                                            'orderby = ?' => $id
                                        ));

        $result = array();

        if ($image) {
            $image = $image->toArray();
            $result['src'] = '/images/' . $image['name'];
            $result['label'] = $image['label_' . $lang];
        }

        $this->getResponse()->setBody(Zend_Json::encode($result));
        $this->getResponse()->setHttpResponseCode(200);
    }

    public function getlangAction() {
        $id = $this->getParam('id', 0);
        $lang = $this->getParam('lang', 'en');
        $imageMapper = Model_Mapper_Image::getInstance();
        $image = $imageMapper->fetchRow(array(
                                            'orderby = ?' => $id
                                        ));
        $result = '';

        if ($image) {
            $image = $image->toArray();
            $result = array('label' => $image['label_' . $lang]);
        }

        $this->getResponse()->setBody(Zend_Json::encode($result));
        $this->getResponse()->setHttpResponseCode(200);
    }

    public function updateAction() {
        $value = Zend_Json::decode($this->getParam('image', '[]'));
        $imageMapper = Model_Mapper_Image::getInstance();
        $image = $imageMapper->fetchRow(array(
                                            'id = ?' => $value['id']
                                        ));
        if ($image) {
            $image->label_en = $value['label_en'];
            $image->label_hu = $value['label_hu'];
            $image->save();
        }
        $result = Zend_Json::encode(array(
                                        'success' => true
                                    ));
        $this->getResponse()->setBody($result);
        $this->getResponse()->setHttpResponseCode(200);
    }

    public function deleteAction() {
        $id = Zend_Json::decode($this->getParam('id', 0));
        $imageMapper = Model_Mapper_Image::getInstance();
        $image = $imageMapper->fetchRow(array(
                                            'id = ?' => $id
                                        ));
        if ($image) {
            unlink(APPLICATION_PATH . '/../public_html/images/' . $image->name);
            $image->delete();
        }
        $result = Zend_Json::encode(array(
                                        'success' => true
                                    ));
        $this->getResponse()->setBody($result);
        $this->getResponse()->setHttpResponseCode(200);
    }
}