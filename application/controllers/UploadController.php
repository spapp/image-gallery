<?php

class UploadController extends Zend_Controller_Action {
    public function init() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
    }

    public function indexAction() {
        $result = array(
            'success' => true
        );

        $upload = new Zend_File_Transfer_Adapter_Http();
        $upload->setDestination(APPLICATION_PATH . '/../public_html/images');

        try {
            $upload->receive();
            $imageMapper = Model_Mapper_Image::getInstance();
            $image = $imageMapper->create(array(
                                              'name' => basename($upload->getFileName()),
                                              'label_en' => $this->getParam('label_en', ''),
                                              'label_hu' => $this->getParam('label_hu', ''),
                                              'orderby' => $imageMapper->getCount()
                                          ));
            $image->save();

        } catch (Zend_File_Transfer_Exception $e) {
            echo $e->message();
            $result['success'] = false;
        }

        $this->getResponse()->setBody(Zend_Json::encode($result));
        $this->getResponse()->setHttpResponseCode(200);
    }
}