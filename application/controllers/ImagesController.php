<?php

class ImagesController extends Zend_Controller_Action {
    public function init() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
    }

    public function getAction() {
        $imageMapper = Model_Mapper_Image::getInstance();

        $result = Zend_Json::encode(array(
                                        'success' => true,
                                        'images' => $imageMapper->fetchAll(null, 'orderby ASC')->toArray()
                                    ));
        $this->getResponse()->appendBody($result);


        $this->getResponse()->setHttpResponseCode(200);
    }

    public function getcountAction() {
        $imageMapper = Model_Mapper_Image::getInstance();

        $result = Zend_Json::encode(array(
                                        'success' => true
                                    ));
        $this->getResponse()->setBody($imageMapper->getCount());
        $this->getResponse()->setHttpResponseCode(200);
    }

    public function moveAction() {
        $order = Zend_Json::decode($this->getParam('order', '[]'));
        $imageMapper = Model_Mapper_Image::getInstance();


        foreach ($order as $index => $id) {
            $image = $imageMapper->fetchRow(array(
                                                'id = ?' => $id
                                            ));
            if ($image) {
                $image->orderby = $index;
                $image->save();
            }
        }

        $result = Zend_Json::encode(array(
                                        'success' => true
                                    ));
        $this->getResponse()->setBody($result);
        $this->getResponse()->setHttpResponseCode(200);
    }

    public function exportAction() {
        $type = $this->getParam('type', '');
        $imageMapper = Model_Mapper_Image::getInstance();
        $this->getResponse()->clearBody();

        if ($type === 'xml') {
            $this->getResponse()->setHeader('Content-Type', 'application/xml;charset=utf-8');
            $this->getResponse()->setBody($imageMapper->toXml());
        } elseif ($type === 'csv') {
            $this->getResponse()->setHeader('Content-Type', 'text/csv;charset=utf-8');
            $this->getResponse()->setBody($imageMapper->toCsv());
        }
        $this->getResponse()->setHeader('Content-Disposition', 'attachment; filename=images.' . $type);
    }
}