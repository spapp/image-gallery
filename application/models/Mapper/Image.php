<?php

class Model_Mapper_Image {
    const TABLE_MODEL_PREFIX = 'Model_DbTable_';

    protected static $_instance = null;
    protected $_dbTable = null;

    protected function __construct() {
    }

    public function setDbTable($aDbTable) {
        if (is_string($aDbTable)) {
            $this->_dbTable = new $aDbTable();
        }
        if (!($this->_dbTable instanceof Zend_Db_Table_Abstract)) {
            throw new Exception('Invalid table data gateway provided');
        }
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $class = array_pop(explode('_', get_class($this)));
            $this->setDbTable(self::TABLE_MODEL_PREFIX . $class);
        }
        return $this->_dbTable;
    }

    public function getCount() {
        $select = $this->getDbTable()->select(true)->columns(array('COUNT(*) AS count'));
        $count = $this->fetchRow($select);
        return $count ? $count->count : 0;
    }

    public function create(array $aData = array(), $aDefaultSource = null) {
        $table = $this->getDbTable();
        return $table->createRow($aData, $aDefaultSource);
    }

    public function find($aId) {
        $result = $this->getDbTable()->find($aId);
        $row = null;

        if (0 < count($result)) {
            $row = $result->current();
        }

        return $row;
    }

    public function fetchAll($aWhere = null, $aOrder = null, $aCount = null, $aOffset = null) {
        return $this->getDbTable()->fetchAll($aWhere, $aOrder, $aCount, $aOffset);
    }

    public function fetchRow($aWhere = null, $aOrder = null, $aOffset = null) {
        return $this->getDbTable()->fetchRow($aWhere, $aOrder, $aOffset);
    }

    public final function save($aModel) {
        if (is_array($aModel)) {
            $aModel = $this->create($aModel);
        }
        return $aModel->save();
    }

    public final function toXml() {
        $images = $this->fetchAll(null, 'orderby ASC');
        $xml = array(
            '<?xml version="1.0" encoding="utf-8"?>',
            '<images>'
        );
        foreach ($images as $image) {
            array_push($xml, '<image>');
            foreach ($image as $key => $value) {
                array_push($xml, '<' . $key . '>' . $value . '</' . $key . '>');
            }
            array_push($xml, '</image>');
        }
        array_push($xml, '</images>');
        return join("\n", $xml);
    }


    public final function toCsv() {
        $images = $this->fetchAll(null, 'orderby ASC');
        $csv = array();
        foreach ($images as $image) {
            $img = array();
            foreach ($image as $value) {
                array_push($img, '"' . $value . '"');
            }
            array_push($csv, join(',', $img));
        }
        return join("\r\n", $csv);
    }


    public static function getInstance() {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
}